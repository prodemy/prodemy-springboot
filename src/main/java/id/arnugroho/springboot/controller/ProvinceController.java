package id.arnugroho.springboot.controller;

import id.arnugroho.springboot.model.dto.ProvinceDto;
import id.arnugroho.springboot.model.entity.Province;
import id.arnugroho.springboot.repository.ProvinceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/province")
public class ProvinceController {
    private final ProvinceRepository provinceRepository;

    @Autowired
    public ProvinceController(ProvinceRepository provinceRepository) {
        this.provinceRepository = provinceRepository;
    }

    // http://localhost:8080/province
    @GetMapping
    public List<ProvinceDto> get() {
        List<Province> provinceList = provinceRepository.findAll();
        List<ProvinceDto> provinceDtoList = provinceList.stream().map(this::convertToDto)
                .collect(Collectors.toList());
        return provinceDtoList;
    }

    // http://localhost:8080/province/33
    @GetMapping("/{code}")
    public ProvinceDto get(@PathVariable String code) {
        if(provinceRepository.findById(code).isPresent()){
            ProvinceDto provinceDto =  convertToDto(provinceRepository.findById(code).get());
            return provinceDto;
        }
        return null;
    }
    /*Insert Data*/
    @PostMapping
    public ProvinceDto insert(@RequestBody ProvinceDto dto) {
        Province province = convertToEntity(dto);
        provinceRepository.save(province);
        return dto;
    }

    private Province convertToEntity(ProvinceDto dto){
        Province province = new Province();
        province.setKodeProvince(dto.getCode());
        province.setNamaProvince(dto.getName());
        return province;
    }

    private ProvinceDto convertToDto(Province province){
        ProvinceDto provinceDto = new ProvinceDto();
        provinceDto.setCode(province.getKodeProvince());
        provinceDto.setName(province.getNamaProvince());
        return provinceDto;
    }
}
