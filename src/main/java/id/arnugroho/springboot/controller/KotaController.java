package id.arnugroho.springboot.controller;

import id.arnugroho.springboot.model.dto.KotaDto;
import id.arnugroho.springboot.model.entity.Kota;
import id.arnugroho.springboot.model.entity.Province;
import id.arnugroho.springboot.repository.KotaRepository;
import id.arnugroho.springboot.repository.ProvinceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/kota")
public class KotaController {
    private final KotaRepository repository;
    private final ProvinceRepository provinceRepository;

    @Autowired
    public KotaController(KotaRepository repository, ProvinceRepository provinceRepository) {
        this.repository = repository;
        this.provinceRepository = provinceRepository;
    }


    // http://localhost:8080/kota
    @GetMapping
    public List<KotaDto> get() {
        List<Kota> kotaList = repository.findAll();
        List<KotaDto> kotaDtoList = kotaList.stream().map(this::convertToDto)
                .collect(Collectors.toList());
        return kotaDtoList;
    }

    // http://localhost:8080/kota/3303
    @GetMapping("/{code}")
    public KotaDto get(@PathVariable String code) {
        if(repository.findById(code).isPresent()){
            KotaDto kotaDto =  convertToDto(repository.findById(code).get());
            return kotaDto;
        }
        return null;
    }

    @GetMapping("/prov/{codeProvince}")
    public List<KotaDto> getByProvince(@PathVariable String codeProvince) {
//        if(repository.findById(code).isPresent()){
//            KotaDto kotaDto =  convertToDto(repository.findById(code).get());
//            return kotaDto;
//        }
        List<Kota> kotaList = repository.findAllByProvinceKodeProvince(codeProvince);
        List<KotaDto> kotaDtoList = kotaList.stream().map(this::convertToDto)
                .collect(Collectors.toList());
        return kotaDtoList;
    }
    /*Insert Data*/
    @PostMapping
    public KotaDto insert(@RequestBody KotaDto dto) {
        Kota kota = convertToEntity(dto);
        repository.save(kota);
        return dto;
    }

    private Kota convertToEntity(KotaDto dto){
        Kota kota = new Kota();
        kota.setKodeKota(dto.getCode());
        kota.setNamaKota(dto.getName());

        if(provinceRepository.findById(dto.getCodeProvince()).isPresent()){
            Province province =  provinceRepository.findById(dto.getCodeProvince()).get();
            kota.setProvince(province);
        }

        return kota;
    }

    private KotaDto convertToDto(Kota kota){
        KotaDto kotaDto = new KotaDto();
        kotaDto.setCode(kota.getKodeKota());
        kotaDto.setName(kota.getNamaKota());
        kotaDto.setCodeProvince(kota.getProvince().getKodeProvince());
        kotaDto.setNamaProvince(kota.getProvince().getNamaProvince());
        return kotaDto;
    }
}
