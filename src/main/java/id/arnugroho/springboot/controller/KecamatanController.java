package id.arnugroho.springboot.controller;

import id.arnugroho.springboot.model.dto.KecamatanDto;
import id.arnugroho.springboot.model.dto.KotaDto;
import id.arnugroho.springboot.model.entity.Kecamatan;
import id.arnugroho.springboot.model.entity.Kota;
import id.arnugroho.springboot.model.entity.Province;
import id.arnugroho.springboot.repository.KecamatanRepository;
import id.arnugroho.springboot.repository.KotaRepository;
import id.arnugroho.springboot.repository.ProvinceRepository;
import id.arnugroho.springboot.service.KecamatanService;
import id.arnugroho.springboot.service.KecamatanServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/kecamatan")
public class KecamatanController {
    private final KecamatanRepository kecamatanRepository;
    private final KotaRepository kotaRepository;
    private final ProvinceRepository provinceRepository;

    @Autowired
    private KecamatanService kecamatanService;

    @Autowired
    public KecamatanController(KecamatanRepository kecamatanRepository, KotaRepository kotaRepository, ProvinceRepository provinceRepository) {
        this.kecamatanRepository = kecamatanRepository;
        this.kotaRepository = kotaRepository;
        this.provinceRepository = provinceRepository;
    }


    // http://localhost:8080/kecamatan
    @GetMapping
    public List<KecamatanDto> get() {
        List<Kecamatan> kotaList = kecamatanRepository.findAll();
        List<KecamatanDto> kotaDtoList = kotaList.stream().map(this::convertToDto)
                .collect(Collectors.toList());
        return kotaDtoList;
    }

    // http://localhost:8080/kecamatan/3303
    @GetMapping("/{code}")
    public KecamatanDto get(@PathVariable String code) {
        if(kecamatanRepository.findById(code).isPresent()){
            KecamatanDto kecamatanDto =  convertToDto(kecamatanRepository.findById(code).get());
            return kecamatanDto;
        }
        return null;
    }

    @GetMapping("/prov/{codeProvince}")
    public List<KecamatanDto> getByProvince(@PathVariable String codeProvince) {
//        List<Kecamatan> kecamatanList = kecamatanRepository.findAllByProveKodeProvince(4);
        List<Kecamatan> kecamatanList = kecamatanRepository.findAllByProveKodeProvince(codeProvince);
//        Province province = provinceRepository.findById(codeProvince).get();
//        List<Kecamatan> kecamatanList = kecamatanRepository.findAllByProve(province);
        List<KecamatanDto> kecamatanDtoList = kecamatanList.stream().map(this::convertToDto)
                .collect(Collectors.toList());
        return kecamatanDtoList;
    }
    /*Insert Data*/
    @PostMapping
    public KecamatanDto insert(@RequestBody KecamatanDto dto) {
        Kecamatan kecamatan = convertToEntity(dto);
        kecamatanRepository.save(kecamatan);
        return convertToDto(kecamatan);
    }

    /*Insert Data*/
    @PostMapping("/trx")
    public KecamatanDto insertTrx(@RequestBody KecamatanDto dto) {
        Kecamatan kecamatan = convertToEntity(dto);
        kecamatanService.insertDataKecamatan(kecamatan);
        return convertToDto(kecamatan);
    }

    /*Insert Data*/
    @PostMapping("/notrx")
    public KecamatanDto insertNoTrx(@RequestBody KecamatanDto dto) {
        Kecamatan kecamatan = convertToEntity(dto);
        Kecamatan entity = kecamatanRepository.save(kecamatan);
        entity.setJumlahPenduduk(4000);
        Integer.parseInt("saya");
        kecamatanRepository.save(entity);
        return convertToDto(kecamatan);
    }

    private Kecamatan convertToEntity(KecamatanDto dto){
        Kecamatan kecamatan = new Kecamatan();
        kecamatan.setKodeKecamatan(dto.getCode());
        kecamatan.setNamaKecamatan(dto.getName());

        if(provinceRepository.findById(dto.getCodeProvince()).isPresent()){
            Province province =  provinceRepository.findById(dto.getCodeProvince()).get();
            kecamatan.setProve(province);
        }
        if(kotaRepository.findById(dto.getCodeKota()).isPresent()){
            Kota kota =  kotaRepository.findById(dto.getCodeKota()).get();
            kecamatan.setKota(kota);
        }

        return kecamatan;
    }

    private KecamatanDto convertToDto(Kecamatan kecamatan){
        KecamatanDto dto = new KecamatanDto();
        dto.setCode(kecamatan.getKodeKecamatan());
        dto.setName(kecamatan.getNamaKecamatan());
        dto.setCodeProvince(kecamatan.getProve().getKodeProvince());
        dto.setNamaProvince(kecamatan.getProve().getNamaProvince());
        dto.setCodeKota(kecamatan.getKota().getKodeKota());
        dto.setNamaKota(kecamatan.getKota().getNamaKota());
        return dto;
    }
}
