package id.arnugroho.springboot.service;

import id.arnugroho.springboot.model.entity.Kecamatan;

public interface KecamatanService {
    Kecamatan insertDataKecamatan(Kecamatan kecamatan);
}
