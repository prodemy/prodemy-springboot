package id.arnugroho.springboot.service;

import id.arnugroho.springboot.model.entity.Kecamatan;
import id.arnugroho.springboot.repository.KecamatanRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class KecamatanServiceImpl implements KecamatanService{
    @Autowired
    private KecamatanRepository kecamatanRepository;

    @Override
    public Kecamatan insertDataKecamatan(Kecamatan kecamatan) {
        Kecamatan entity = kecamatanRepository.save(kecamatan);
        entity.setJumlahPenduduk(6000);
        Integer.parseInt("kamu");
        return kecamatanRepository.save(entity);
    }

    public void cariKecamatan(){

    }
}
