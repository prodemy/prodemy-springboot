package id.arnugroho.springboot.model.entity;

import javax.persistence.*;

@Entity
@Table(name = "t_kota")
public class Kota {
    @Id
    @Column(name = "kdkota", length = 25)
    private String kodeKota;

    @Column(name = "nmkota")
    private String namaKota;

    @ManyToOne
    @JoinColumn(name="kdprov", nullable = false)
    private Province province;

    public String getKodeKota() {
        return kodeKota;
    }

    public void setKodeKota(String kodeKota) {
        this.kodeKota = kodeKota;
    }

    public String getNamaKota() {
        return namaKota;
    }

    public void setNamaKota(String namaKota) {
        this.namaKota = namaKota;
    }

    public Province getProvince() {
        return province;
    }

    public void setProvince(Province province) {
        this.province = province;
    }
}
