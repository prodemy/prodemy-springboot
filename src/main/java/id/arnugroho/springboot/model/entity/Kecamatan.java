package id.arnugroho.springboot.model.entity;

import javax.persistence.*;

@Entity
@Table(name = "t_kecamatan")
public class Kecamatan {
    @Id
    @Column(name = "kdkecamatan", length = 25)
    private String kodeKecamatan;

    @Column(name = "nmkecamatan")
    private String namaKecamatan;

    @ManyToOne
    @JoinColumn(name="kdkota", nullable = false)
    private Kota kota;

    @ManyToOne
    @JoinColumn(name="kdprov", nullable = false)
    private Province prove;

    @Column(name="jmlpenduduk")
    private Integer jumlahPenduduk;

    public String getKodeKecamatan() {
        return kodeKecamatan;
    }

    public void setKodeKecamatan(String kodeKecamatan) {
        this.kodeKecamatan = kodeKecamatan;
    }

    public String getNamaKecamatan() {
        return namaKecamatan;
    }

    public void setNamaKecamatan(String namaKecamatan) {
        this.namaKecamatan = namaKecamatan;
    }

    public Kota getKota() {
        return kota;
    }

    public void setKota(Kota kota) {
        this.kota = kota;
    }

    public Province getProve() {
        return prove;
    }

    public void setProve(Province prove) {
        this.prove = prove;
    }

    public Integer getJumlahPenduduk() {
        return jumlahPenduduk;
    }

    public void setJumlahPenduduk(Integer jumlahPenduduk) {
        this.jumlahPenduduk = jumlahPenduduk;
    }
}
