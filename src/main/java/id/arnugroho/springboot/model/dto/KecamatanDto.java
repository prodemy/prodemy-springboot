package id.arnugroho.springboot.model.dto;

public class KecamatanDto {
    private String code;
    private String name;
    private String codeProvince;
    private String namaProvince;
    private String codeKota;
    private String namaKota;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCodeProvince() {
        return codeProvince;
    }

    public void setCodeProvince(String codeProvince) {
        this.codeProvince = codeProvince;
    }

    public String getNamaProvince() {
        return namaProvince;
    }

    public void setNamaProvince(String namaProvince) {
        this.namaProvince = namaProvince;
    }

    public String getCodeKota() {
        return codeKota;
    }

    public void setCodeKota(String codeKota) {
        this.codeKota = codeKota;
    }

    public String getNamaKota() {
        return namaKota;
    }

    public void setNamaKota(String namaKota) {
        this.namaKota = namaKota;
    }
}
