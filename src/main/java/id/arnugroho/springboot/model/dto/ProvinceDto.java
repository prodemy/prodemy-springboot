package id.arnugroho.springboot.model.dto;

/*
* Disini kita akan merepresentasikan data provinsi
* contoh data yang diinginkan adalah
* {"code":"32","name":"Jawa Barat","code":"33","name":"Jawa Tengah",...}
* sehingga kita membutuhkan code dan name sebagai nama atributnya
* */
public class ProvinceDto {
    private String code;
    private String name;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
